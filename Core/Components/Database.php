<?php

namespace Aelast\Aecmf\Core\Components;

use \Illuminate\Database\Capsule\Manager as Capsule;

class Database
{

    private static $capsule;

    public static function init($config)
    {
        static::$capsule = new Capsule;
        static::$capsule->addConnection($config);
        static::$capsule->bootEloquent();
    }

}
