<?php

namespace Aelast\Aecmf\Core\Components;

use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use ArrayIterator;

class Collection extends EloquentCollection
{

    protected $iterator;

    /**
     * Get an iterator for the items.
     *
     * @return Core\Components\GreedArrayIterator
     */
    public function getIterator()
    {
        $this->iterator = new ArrayIterator($this->items);
        return $this->iterator;
    }

    public function isOrdinalNumber($number)
    {
        return (((int) $this->iterator->key() + 1) % $number) == 0;
    }

    public function isOdd()
    {
        return !$this->isOrdinalNumber(2);
    }

    public function isEven()
    {
        return $this->isOrdinalNumber(2);
    }

}
