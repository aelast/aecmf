<?php

namespace Aelast\Aecmf\Core\Components;

use ArrayObject;

class Response extends ArrayObject
{

    private static $instance;
    private $errors = array();
    private $warnings = array();
    private $messages = array();
    private $stored_data = array();

    public function __construct()
    {
        if (isset(self::$instance)) {
            return self::$instance;
        }
        if (!empty($_SESSION['RESPONSE_STORED'])) {
            $this->stored_data = $_SESSION['RESPONSE_STORED'];
            unset($_SESSION['RESPONSE_STORED']);
        }
    }

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self(self::STD_PROP_LIST);
        }
        return self::$instance;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function setError($message)
    {
        $this->errors[] = $message;
        return $this;
    }

    public function getErrorsCount()
    {
        return count($this->errors);
    }

    public function getLastError()
    {
        if ($this->getErrorsCount() > 0) {
            return $this->errors[$this->getErrorsCount() - 1];
        }
        return null;
    }

    public function getWarnings()
    {
        return $this->warnings;
    }

    public function setWarnings($message)
    {
        $this->warnings[] = $message;
        return $this;
    }

    public function getWarningsCount()
    {
        return count($this->warnings);
    }

    public function getLastWarning()
    {
        if ($this->getWarningsCount() > 0) {
            return $this->warnings[$this->getWarningsCount() - 1];
        }
        return null;
    }

    public function getMessages()
    {
        return $this->messages;
    }

    public function setMessage($message)
    {
        $this->messages[] = $message;
        return $this;
    }

    public function getMessagesCount()
    {
        return count($this->messages);
    }

    public function store($name, $value)
    {
        $_SESSION['RESPONSE_STORED'][$name] = $value;
        return $this;
    }

    public function restore($name)
    {
        if (isset($this->stored_data[$name])) {
            return $this->stored_data[$name];
        }
        return null;
    }

    public function __get($name)
    {
        if (!$this->offsetExists($name)) {
            return null;
        }
        return $this->offsetGet($name);
    }

    public function __isset($name)
    {
        return $this->offsetExists($name);
    }

    public function __set($name, $value)
    {
        $this->offsetSet($name, $value);
        return $this;
    }

    public function __unset($name)
    {
        $this->offsetUnset($name);
        return $this;
    }

}
