<?php

namespace Core\Models;

class DataValidator {
    private $errors = array();
    
    // Метод валидации данных
    public function validate($value, $type, $pattern) {
        if(!empty($pattern)) {
            $result = $this->customValidate($value, $pattern);
        }
        else {
            $method = 'is' . ucfirst($type);
            if(method_exists($this, $method)) {
                $result = $this->$method($value);
            }
            else {
                $result = false;
            }
        }
        if(!$result) {
            $this->errors[] = 'Invalid value "' . $value . '" for type "' . $type . '"';
        }
        return $result;
    }
    
    public function getErrorsCount() {
        return count($this->errors);
    }
    
    public function getErrors() {
        return $this->errors;
    }
    
    public function isString($data) {
        return is_string($data['value']);
    }
    
    public function isInt($data) {
        return (empty($data['value']) || preg_match('/^[1-9]\d*$/', $data['value']));
    }
    
    public function isInteger($data) {
        return $this->isInt($data);
    }
    
    public function isNumeric($data) {
        return (empty($data['value']) || preg_match('/^\d*\.\d*$/', $data['value']));
    }
    
    public function isObject($data) {
        return is_object($data['value']);
    }
    
    public function isFlag($data) {
        return ($data['value'] === 0 || $data['value'] === 1);
    }
    
    public function isDatetime($data) {
        
    }
    
    public function customValidate($value, $pattern) {
        return true == preg_match($pattern, $value);
    }
}
