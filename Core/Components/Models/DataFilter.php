<?php

namespace Core\Models;

class DataFilter {
    /**
     * Класс фильтрации данных
     * 
     * Изменяет значение указанное в $value
     * в соответствии с фильтрами, указанными в $field->filters,
     * а также в соответствии с типом данных
     * Возможные значения фильтра: 
     * trim - удаление пробелов вначале и в конце
     * ucfirst - изенение регистра первого сивола
     * lower - изменение в нижний регистр
     * upper - изменение в верхний регистр
     * 
     * Автоматически устанавливает значение 0 или 1 для флагов,
     * приводит к типам integer и numeric (float),
     * преобразует массивы и строки в объекты и обратно
     * 
     * @param mixed $value
     * @param Core\Models\ModelField $field
     * @return mixed $value
     */
    public function modify($value, $type, Array $filters = array(), $normalize = true) {
        if($normalize) {
            $newValue = $this->normalizeValueByType($value, $type);
        }
        else {
            $newValue = $value;
        }
        foreach($filters as $filter) {
            $method = 'filter' . ucfirst($filter);
            if(method_exists($this, $method)) {
                $newValue = $this->$method($newValue);
            }
        }
        return $newValue;
    }
    
    private function normalizeValueByType($value, $type) {
        if($type == 'flag') {
            $newValue = empty($value) ? 0 : 1;
        }
        elseif($type == 'integer' || $type == 'int') {
            $newValue = (int)$value;
        }
        elseif($type == 'numeric' || $type == 'float') {
            $newValue = (float)str_replace(',', '.', $value);
        }
        elseif($type == 'object') {
            $newValue = $this->toObject($value);
        }
        return $newValue;
    }
    
    public function toObject($value) {
        if(is_string($value)) {
            try {
                $newValue = json_decode($value);
            }
            catch(\Exception $e) { }
        }
        elseif(is_array($value)) {
            $newValue = json_decode(json_encode($value));
        }
        if(!is_object($newValue)) {
            $newValue = new \stdClass();
        }
        return $newValue;
    }
    
    public function filterTrim($string) {
        return trim($string);
    }
    
    public function filterUcfirst($string) {
        return ucfirst($string);
    }
    
    public function filterLower($string) {
        return strtolower($string);
    }

    public function filterUpper($string) {
        return strtoupper($string);
    }
    
    public function filterArray2object($array) {
        if(is_array($array)) {
            return json_decode(json_encode($array));
        }
        else {
            return new \stdClass();
        }
    }
}
