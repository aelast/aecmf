<?php

namespace Aelast\Aecmf\Core\Components;

use Aelast\Tools\Files as Files;
use Aelast\Aecmf\Core\Events\Event as Event;
use Aelast\Aecmf\Core\Application as App;

class ComponentsLoader
{

    private $module;
    private $path;
    private $tools;
    private $types = [
        'controller',
        'service',
        'widget'
    ];
    private $components = [];

    public function __construct($module)
    {
        $this->module = $module;
        $this->path = $module->getPath();
        $this->tools = Tools::getInstance();
        foreach ($this->types as $type) {
            $this->loadComponentsList($type);
        }
    }

    private function loadComponentsList($type)
    {
        $component_type = ucfirst($type);
        $path = $this->path . '/' . $component_type . 's/';
        if (!$files = Files::getList($path)) {
            return false;
        }
        $name_space = '\\' . str_replace('/', '\\', $this->path);
        foreach ($files as $file) {
            if (strpos($file['basename'], '.php' === false)) {
                continue;
            }
            $component = new \stdClass();
            $component->type = $component_type;
            $component->name = $file['filename'];
            $component->path = $path;
            $component->file = $path . $file['basename'];
            $component->class = $name_space . '\\' . $component_type . 's\\' . $file['filename'];
            $this->components[$component->name][$component->type . 's'] = $component;
        }
    }

    private function registerComponentEvents($component, $component_name)
    {
        if (!method_exists($component, 'getActionsList')) {
            return;
        }
        foreach ($component->getActionsList() as $action) {
            App::getEventManager()->setEvent(
                new Event(array(Event::BEFORE, $this->module->getName(), $component_name, $action))
            )->setEvent(
                new Event(array(Event::AFTER, $this->module->getName(), $component_name, $action))
            );
        }
    }

    public function loadComponent($type, $name, $params = null)
    {
        $component_type = ucfirst($type);
        if (empty($this->components[$name][$component_type . 's'])) {
            return false;
        }
        $component = $this->components[$name][$component_type . 's'];
        require_once $component->file;
        if (!class_exists($component->class, false)) {
            unset($this->components[$name][$component_type . 's']);
            return false;
        }
        $instance = new $component->class($this->module, $params);
        $this->registerComponentEvents($component, $name);
        return $instance;
    }

}
