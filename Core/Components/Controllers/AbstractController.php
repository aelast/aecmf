<?php

namespace Aelast\Aecmf\Core\Components\Controllers;

use Aelast\Aecmf\Core\Application as App;
use Aelast\Aecmf\Core\Events\Event as Event;

abstract class AbstractController
{

    protected $module;
    protected $response;

    public function __construct($module)
    {
        $this->module = $module;
        $this->response = App::getResponse();
    }

    protected function raiseEvent($when, $module_name, $controller_name, $action_name)
    {
        App::getEventsManager()->raiseEvent(array(
            $when,
            $module_name,
            $controller_name,
            $action_name
        ));
    }

    public function getActionsList()
    {
        $actions = array();
        try {
            $methods = get_class_methods($this);
        } catch (Aelast\Aecmf\Core\CmfException $e) {
            unset($e);
            return $actions;
        }
        foreach ($methods as $method) {
            $pos = strpos($method, 'Action');
            if ($pos === false) {
                continue;
            }
            $action = substr_replace($method, '', $pos);
            if ($method !== $action . 'Action') {
                continue;
            }
            $actions[] = $action;
        }
        return $actions;
    }

    public function dispatch($module_name, $controller_name, $action_name)
    {
        $this->raiseEvent(Event::BEFORE, $module_name, $controller_name, $action_name);
        if (App::getResponse()->getErrorsCount() > 0) {
            return;
        }
        $method = $action_name . 'Action';
        if (!is_callable(array($this, $method))) {
            throw new \Core\CmfException('Metod "' . $method . '" is not callable');
        }
        $this->$method();
        if (App::getResponse()->getErrorsCount() > 0) {
            return;
        }
        $this->raiseEvent(Event::AFTER, $module_name, $controller_name, $action_name);
    }

}
