<?php

namespace Aelast\Aecmf\Core\Components;

use ArrayObject;

class Request
{

    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';
    const METHOD_PUT = 'PUT';
    const METHOD_DELETE = 'DELETE';
    const PROTOCOL_HTTP = 'http';
    const PROTOCOL_HTTPS = 'https';

    protected static $method;
    protected static $protocol;
    protected static $host;
    protected static $port = 80;
    protected static $path;
    protected static $query;
    protected static $base_url;
    protected static $base_path;
    protected static $url;
    protected static $uri;
    protected static $server;
    protected static $headers;
    protected static $get;
    protected static $post;
    protected static $files;
    protected static $cookies;

    protected static function setGet()
    {
        if (!is_null(static::$get)) {
            return;
        }
        static::$get = new ArrayObject((array) $_GET, ArrayObject::ARRAY_AS_PROPS);
    }

    protected static function setPost()
    {
        if (!is_null(static::$post)) {
            return;
        }
        static::$post = new ArrayObject((array) $_POST, ArrayObject::ARRAY_AS_PROPS);
    }

    protected static function setFiles()
    {
        if (!is_null(static::$files)) {
            return;
        }
        static::$files = new ArrayObject((array) $_FILES, ArrayObject::ARRAY_AS_PROPS);
    }

    protected static function setCookies()
    {
        if (!is_null(static::$cookies)) {
            return;
        }
        static::$cookies = new ArrayObject((array) $_COOKIE, ArrayObject::ARRAY_AS_PROPS);
    }

    protected static function setServer()
    {
        if (!is_null(static::$server)) {
            return;
        }
        static::$server = new ArrayObject((array) $_SERVER, ArrayObject::ARRAY_AS_PROPS);
    }

    protected static function setMethod()
    {
        if (!is_null(static::$method)) {
            return;
        }
        $method = static::getServer('REQUEST_METHOD', 'UNDEFINED');
        if (defined('static::METHOD_' . $method)) {
            static::$method = $method;
            return;
        }
        static::$method = static::METHOD_GET;
    }

    protected static function setProtocol()
    {
        if (!is_null(static::$protocol)) {
            return;
        }
        if ((!empty(static::getServer('HTTPS')) && strtolower(static::getServer('HTTPS')) !== 'off') || (!empty(static::getServer('HTTP_X_FORWARDED_PROTO')) && static::getServer('HTTP_X_FORWARDED_PROTO') == 'https')
        ) {
            $protocol = static::PROTOCOL_HTTPS;
        } else {
            $protocol = static::PROTOCOL_HTTP;
        }
        static::$protocol = $protocol;
    }

    protected static function setHeaders()
    {
        if (!is_null(static::$headers)) {
            return;
        }
        $headers = [];
        foreach (static::getServer(null, []) as $key => $value) {
            if (!$value && (is_array($value) || !strlen($value))) {
                continue;
            }
            if (strpos($key, 'HTTP_') === 0 && strpos($key, 'HTTP_COOKIE') !== 0) {
                $headers[strtr(ucwords(strtolower(strtr(substr($key, 5), '_', ' '))), ' ', '-')] = $value;
            }
            if (strpos($key, 'CONTENT_') === 0) {
                $name = substr($key, 8); // Remove "Content-"
                $headers['Content-' . ($name == 'MD5' ? $name : ucfirst(strtolower($name)))] = $value;
            }
        }
        static::$headers = new ArrayObject($headers, ArrayObject::ARRAY_AS_PROPS);
    }

    protected static function setHost()
    {
        if (!is_null(static::$host)) {
            return;
        }
        $host = static::getServer('SERVER_NAME', '');
        $port = (int) static::getServer('SERVER_PORT', static::$port);
        if (!empty(static::getServer('SERVER_ADDR')) && preg_match('/^\[[0-9a-fA-F\:]+\]$/', $host)) {
            $host = '[' . static::getServer('SERVER_ADDR') . ']';
            if ($port . ']' == substr($host, strrpos($host, ':') + 1)) {
                $port = null;
            }
        }
        static::$host = $host;
        static::$port = $port;
    }

    protected static function setUri()
    {
        if (!is_null(static::$uri)) {
            return;
        }
        $uri = null;
        // Check this first so IIS will catch.
        if (!is_null(static::getServer('HTTP_X_REWRITE_URL'))) {
            $uri = static::getServer('HTTP_X_REWRITE_URL');
        }
        // Check for IIS 7.0 or later with ISAPI_Rewrite
        if (!is_null(static::getServer('HTTP_X_ORIGINAL_URL'))) {
            $uri = static::getServer('HTTP_X_ORIGINAL_URL');
        }
        // IIS7 with URL Rewrite: make sure we get the unencoded url
        // (double slash problem).
        if (static::getServer('IIS_WasUrlRewritten') == '1' && !empty(static::getServer('UNENCODED_URL'))) {
            $uri = static::getServer('UNENCODED_URL');
        }
        // HTTP proxy requests setup request URI with scheme and host [and port]
        // + the URL path, only use URL path.
        if (empty(static::getServer('HTTP_X_REWRITE_URL'))) {
            $uri = static::getServer('REQUEST_URI');
        }
        if ($uri !== null) {
            $uri = preg_replace('#^[^/:]+://[^/]+#', '', $uri);
        }
        // IIS 5.0, PHP as CGI.
        if (!empty(static::getServer('ORIG_PATH_INFO'))) {
            $uri = static::getServer('ORIG_PATH_INFO') . (!empty(static::getServer('QUERY_STRING')) ? '?' . static::getServer('QUERY_STRING') : '');
        }
        static::$uri = !empty($uri) ? $uri : '/';
    }

    protected static function setPath()
    {
        if (!is_null(static::$path)) {
            return;
        }
        $uri = static::getUri();
        if (($qpos = strpos($uri, '?')) !== false) {
            $uri = substr($uri, 0, $qpos);
        }
        static::$path = $uri;
    }

    protected static function setQuery()
    {
        if (!is_null(static::$query)) {
            return;
        }
        static::$query = static::getServer('QUERY_STRING', '');
    }

    protected static function setUrl()
    {
        if (!is_null(static::$url)) {
            return;
        }
        static::$url = static::getProtocol() . '://' . static::getHost() . static::getUri();
    }

    protected static function setBaseUrl()
    {
        if (!is_null(static::$base_url)) {
            return;
        }
        $filename = static::getServer('SCRIPT_FILENAME', '');
        $scriptName = static::getServer('SCRIPT_FILENAME');
        $phpSelf = static::getServer('PHP_SELF');
        $origScriptName = static::getServer('ORIG_SCRIPT_NAME');
        if ($scriptName !== null && basename($scriptName) === $filename) {
            $baseUrl = $scriptName;
        } elseif ($phpSelf !== null && basename($phpSelf) === $filename) {
            $baseUrl = $phpSelf;
        } elseif ($origScriptName !== null && basename($origScriptName) === $filename) {
            $baseUrl = $origScriptName;
        } else {
            $baseUrl = '/';
            $basename = basename($filename);
            if ($basename) {
                $path = ($phpSelf ? trim($phpSelf, '/') : '');
                $basePos = strpos($path, $basename) ? : 0;
                $baseUrl .= substr($path, 0, $basePos) . $basename;
            }
        }
        // Does the base URL have anything in common with the request URI?
        $uri = static::getUri();
        // Full base URL matches.
        if (0 === strpos($uri, $baseUrl)) {
            static::$base_url = $baseUrl;
            return;
        }
        // Directory portion of base path matches.
        $baseDir = str_replace('\\', '/', dirname($baseUrl));
        if (0 === strpos($uri, $baseDir)) {
            static::$base_url = $baseDir;
            return;
        }
        $truncatedUri = $uri;
        if (false !== ($pos = strpos($uri, '?'))) {
            $truncatedUri = substr($uri, 0, $pos);
        }
        $basename = basename($baseUrl);
        if (empty($basename) || false === strpos($truncatedUri, $basename)) {
            static::$base_url = '';
            return;
        }
        // If using mod_rewrite or ISAPI_Rewrite strip the script filename
        // out of the base path. $pos !== 0 makes sure it is not matching a
        // value from PATH_INFO or QUERY_STRING.
        if (strlen($uri) >= strlen($baseUrl) && (false !== ($pos = strpos($uri, $baseUrl)) && $pos !== 0)
        ) {
            $baseUrl = substr($uri, 0, $pos + strlen($baseUrl));
        }
        static::$base_url = $baseUrl;
    }

    protected static function setBasePath()
    {
        if (!is_null(static::$base_path)) {
            return;
        }
        $filename = static::getServer('SCRIPT_FILENAME');
        $baseUrl = static::getBaseUrl();
        // Empty base url detected
        if ($baseUrl === '') {
            static::$base_path = '';
            return;
        }
        // basename() matches the script filename; return the directory
        if (basename($baseUrl) === $filename) {
            static::$base_path = str_replace('\\', '/', dirname($baseUrl));
            return;
        }
        // Base path is identical to base URL
        static::$base_path = $baseUrl;
    }

    public static function getMethod()
    {
        static::setMethod();
        return static::$method;
    }

    public static function getProtocol()
    {
        static::setProtocol();
        return static::$protocol;
    }

    public static function getHost()
    {
        static::setHost();
        return static::$host;
    }

    public static function getPort()
    {
        static::setHost();
        return static::$port;
    }

    public static function getPath()
    {
        static::setPath();
        return static::$path;
    }

    public static function getQuery()
    {
        static::setQuery();
        return static::$query;
    }

    public static function getUri()
    {
        static::setUri();
        return static::$uri;
    }

    public static function getUrl()
    {
        static::setUrl();
        return static::$url;
    }

    public static function getBaseUrl()
    {
        static::setBaseUrl();
        return static::$base_url;
    }

    public static function getBasePath()
    {
        static::setBasePath();
        return static::$base_path;
    }

    public static function post($name, $default = null)
    {
        static::setPost();
        if (isset(static::$post[$name])) {
            return static::$post[$name];
        }
        return $default;
    }

    public static function get($name, $default = null)
    {
        static::setGet();
        if (isset(static::$get[$name])) {
            return static::$get[$name];
        }
        return $default;
    }

    public static function getAllParams()
    {
        static::setGet();
        $result = array();
        foreach (static::$get as $key => $value) {
            $result[$key] = $value;
        }
        return $result;
    }

    public static function cookies($name, $default = null)
    {
        static::setCookies();
        if (isset(static::$cookies[$name])) {
            return static::$cookies[$name];
        }
        return $default;
    }

    public static function files($name, $default = null)
    {
        static::setFiles();
        if (isset(static::$files[$name])) {
            return static::$files[$name];
        }
        return $default;
    }

    public static function getServer($name = null, $default = null)
    {
        static::setServer();
        if ($name === null) {
            return static::$server;
        }
        if (isset(static::$server[$name])) {
            return static::$server[$name];
        }
        return $default;
    }

    public static function getHeaders($name = null, $default = null)
    {
        static::setHeaders();
        if ($name === null) {
            return static::headers;
        }
        if (isset(static::$headers[$name])) {
            return static::$headers[$name];
        }
        return $default;
    }

    public static function isAjax()
    {
        $header = static::getHeaders('X-Requested-With', false);
        return false !== $header && $header == 'XMLHttpRequest';
    }

}
