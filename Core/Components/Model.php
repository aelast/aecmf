<?php

namespace Aelast\Aecmf\Core\Components;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use Aelast\Aecmf\Core\Components\Collection as Collection;

class Model extends EloquentModel
{

    /**
     * Create a new Collection instance.
     *
     * @param  array  $models
     * @return \Core\Components\Collection
     */
    public function newCollection(array $models = [])
    {
        return new Collection($models);
    }

}
