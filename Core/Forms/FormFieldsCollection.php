<?php

namespace Core\Forms;

class FormFieldsCollection {
    private static $instance;
    
    private $collection = array();
    private $defaultField = 'string';
    
    private function __construct() {
        
    }
    
	public static function getInstance() {
		if(!isset(self::$instance)) {
			self::$instance = new self();
		}
		return self::$instance;
	}
    
    public function get($fieldName, $returnDefault = true) {
        if(isset($this->collection[$fieldName])) {
            return $this->collection[$fieldName];
        }
        if($returnDefault) {
            return $this->collection[$this->defaultField];
        }
        return null;
    }
    
    public function set($name, $field) {
        if(isset($this->collection[$name])) {
            
        }
    }
}
