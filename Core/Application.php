<?php

namespace Aelast\Aecmf\Core;

use Aelast\Aecmf\Core\Components\Request as Request;
use Aelast\Aecmf\Core\Components\Response as Response;
use Aelast\Aecmf\Core\Routers\Router as Router;
use Aelast\Aecmf\Core\View\ViewFactory as ViewFactory;
use Aelast\Aecmf\Core\Modules\ModulesManager as ModulesManager;
use Aelast\Aecmf\Core\Components\Database as DB;
use Aelast\Aecmf\Core\Events\EventsManager as EventsManager;
use Aelast\Aecmf\Core\Events\Event as Event;
use Aelast\Aecmf\Core\Config\Config as Config;

/**
 * Основной класс приложения
 * 
 * Инициализирует основные компоненты ядра,
 * обеспечивает доступ к этим компонентам.
 * Запускает контроллер основного действия, указанного в router
 * Инициализируется с помощью статичного метода run
 * 
 * @param string $appName имя приложения
 * 
 */
class Application
{

    /**
     * Имя общей части конфигурации
     * 
     * По факту может применяться только в конфигурационных файлах
     * для определения общих секций разных типов приложения
     */
    const APP_COMMON = 'common';

    /**
     * Имя публичной части конфигурации или типа приложения
     * 
     * Публичное приложение не использует проверку прав пользователя
     * и не обращает внимание на авторизацию пользователя, т.е. доступно всем
     */
    const APP_PUBLIC = 'public';

    /**
     * Имя защищенной части конфигурации или типа приложения
     * 
     * Защищенное приложение использует только проверку авторизации пользователя
     * и не использует проверку прав пользователя. Может быть использованно там,
     *  где нет разграничения прав пользователей, например, в личном кабинете
     */
    const APP_PROTECTED = 'protected';

    /**
     * Имя приватной части конфигурации или типа приложения
     * 
     * Приватное приложение использует как проверку прав пользователя,
     * так и проверку авторизации. Применимо там где требуется ограничивать права пользователей,
     * например в административной части сайта или CRM
     */
    const APP_PRIVATE = 'private';

    /**
     * Имя события вызова контроллера
     * 
     * Используется для генерации событий до и после вызова action-метода основного контроллера
     */
    const RUN_ACTION = 'runAction';

    /**
     * Имя приложения
     * 
     * Имя каталога в каталоге Applications,
     * конфигурация из которого будет использована для работы приложения
     * 
     * @var string
     */
    private static $name;

    /**
     * Объект-коллектор исходящих данных
     * 
     * Синглтон, аккумулирует данные для ответа от сервера
     * 
     * @var Aelast\Aecmf\Core\Components\Response
     */
    private static $response;

    /**
     * Объект управления модулями системы
     * 
     * Является фабрикой и хранилищем модулей
     * 
     * @var Core\Module\ModulesManager
     */
    private static $modules_manager;

    /**
     * Менеджер событий
     * 
     * Обеспечивает хранение и возбуждение событий,
     * установку слушателей событий
     * 
     * @var Aelast\Aecmf\Core\Modules\ModulesManager
     */
    private static $events_manager;

    /**
     * Представление
     * 
     * Обеспечивает передачу ответа из Response в макет,
     * рендер макета
     * 
     * @var object
     */
    private static $view;

    /**
     * Тип приложения
     * 
     * По умолчанию установлен в self::APP_PRIVATE, в конструкторе указывается
     * реальный тип приложения, установленный в config-файле приложения
     * 
     * @var self::APP_PRIVATE|self::APP_PROTECTED|self::APP_PUBLIC
     */
    private static $type = self::APP_PRIVATE;
    private static $initialized = false;

    /**
     * Статичный метод инициализации приложения
     * 
     * @param string $app_name имя приложения
     */
    public static function run($app_name)
    {
        if (static::$initialized === true) {
            return;
        }
        static::$initialized = true;
        static::$name = $app_name;
        static::init();
    }

    /**
     * Конструктор класса
     * 
     * Может быть вызван только из статичного метода run()
     * Вызывает метод загрузки сервисов,
     * запускает action контроллера,
     * выполняет render()
     * 
     * @param string $appName
     */
    private static function init()
    {
        // Загрузим необходимые сервисы
        static::loadServices();
        // Запустим action
        static::runAction(
            Router::getModule(), Router::getController(), Router::getAction()
        );
        static::$response->memory_usage = memory_get_usage() / 1024;
        static::$response->real_memory_usage = memory_get_usage(true) / 1024;
        static::$response->peak_memory_usage = memory_get_peak_usage() / 1024;
        static::$response->real_peak_memory_usage = memory_get_peak_usage(true) / 1024;
        static::$view->show();
    }

    /**
     * Метод загрузки основных сервисов ядра
     * 
     * Загружает:
     * Core\Components\Response
     * Core\Routers\RouterFactory
     * Core\View\ViewFactory
     * Core\Module\ModulesManager
     * Core\Database\DatabaseManager
     * Core\Events\EventsManager
     * Core\Config\Config
     */
    private static function loadServices()
    {
        if (!empty(Request::post(session_name())) && !session_id()) {
            session_id(Request::post(session_name()));
        }
        session_start();
        static::$response = Response::getInstance();
        static::loadConfig();
        DB::init(Config::get('database', []));
        Config::remove('database');
        static::$events_manager = static::loadEventsManager();
        static::$modules_manager = ModulesManager::getInstance($this);
        Router::init(Config::get('router'));
        static::$view = static::loadView();
    }

    private static function loadConfig()
    {
        if (!file_exists('../Applications/' . static::$name . '/config.php')) {
            throw new CmfException('Configuration for application "' . static::$name . '" not exists!');
        }
        if (file_exists('../Common/config.php')) {
            Config::load(require '../Common/config.php');
        }
        Config::load(require '../Applications/' . static::$name . '/config.php');
        $app_type = Config::get('app_type', null);
        if (is_null($app_type)) {
            throw new CmfException('Application type is missing!');
        }
        static::$type = $app_type;
    }

    private static function loadEventsManager()
    {
        $events_manager = EventsManager::getInstance();
        $events_manager->
            setEvent(new Event([Event::BEFORE, 'Application', 'Application', static::RUN_ACTION]))->
            setEvent(new Event([Event::AFTER, 'Application', 'Application', static::RUN_ACTION]));
        return $events_manager;
    }

    private static function loadView()
    {
        $viewInstance = ViewFactory::getView(Config::get('view'));
        return $viewInstance;
    }

    public static function runAction($module, $controller, $action)
    {
        static::$events_manager->raiseEvent([
            Event::BEFORE,
            'Application',
            'Application',
            static::RUN_ACTION
        ]);
        $c = static::$modules_manager->get($module)->getController($controller);
        $c->dispatch($module, $controller, $action);
        static::$events_manager->raiseEvent([
            Event::AFTER,
            'Application',
            'Application',
            self::RUN_ACTION
        ]);
    }

    public static function getName()
    {
        return static::$name;
    }

    public static function getType()
    {
        return static::$type;
    }

    public static function getResponse()
    {
        return static::$response;
    }

    public static function getModulesManager()
    {
        return static::$modules_manager;
    }

    public static function getEventsManager()
    {
        return static::$events_manager;
    }

    public static function getView()
    {
        return static::$view;
    }

}
