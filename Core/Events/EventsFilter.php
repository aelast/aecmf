<?php

namespace Aelast\Aecmf\Core\Events;

use Aelast\Aecmf\Core\Application as App;
use Aelast\Aecmf\Core\Events\Event;

class EventsFilter extends Event
{

    private $events;
    private $filtered_events = null;
    private $mask_symbols = '*!@,';

    public function __construct($events, $params)
    {
        parent::__construct($params);
        $this->events = $events;
    }

    private function filterEvents($listenerName)
    {
        $this->filtered_events = array();
        foreach ($this->events as $eventName => $event) {
            if ($event->issetListener($listenerName)) {
                continue;
            }
            if ($this->isMatchedKeys($eventName)) {
                $this->filtered_events[] = $event;
            }
        }
    }

    private function isMatchedKeys($event_name)
    {
        $keys = explode(Event::SEPARATOR, $this->getName());
        $event_keys = explode(Event::SEPARATOR, $event_name);
        for ($i = 0; $i < count($keys); $i++) {
            $masked = strpbrk($keys[$i], $this->mask_symbols);
            if ($keys[$i] !== $event_keys[$i] && $masked === false) {
                return false;
            }
            if ($masked && !$this->isMaskCondition($keys[$i], $event_keys[$i], $i)) {
                return false;
            }
        }
        return true;
    }

    private function isMaskCondition($mask, $value, $segment)
    {
        // Все варианты
        if ($mask === '*') {
            return true;
        }
        // Для before/after не может быть ничего, кроме звездочки или явного указания
        if ($segment === 0) {
            return $mask === $value;
        }
        // Для перечислений, разобьем на массив
        if (strpos($mask, ',') !== false) {
            $masks = explode(',', $mask);
            foreach ($masks as $m) {
                if (true === $this->isMaskCondition($m, $value, $segment)) {
                    return true;
                }
            }
            return false;
        }

        $pos = strpos($mask, '@');
        // Для контроллера и действия не могут указываться проверки родителей
        if ($pos !== false && ($segment === 2 || $segment === 3)) {
            $mask = substr_replace($mask, '', $pos);
        } elseif ($pos !== false && $segment == 1) {
            return $this->isInstanceCondition($mask, $value);
        }

        // Если первый символ "!" - это запрет значения
        if (strpos($mask, '!') === 0) {
            if (str_replace('!', '', $mask) !== $value) {
                return true;
            }
        }
        return $mask === $value;
    }

    private function isInstanceCondition($mask, $value)
    {
        $striped_mask = str_replace('@', '', $mask);
        if (strpos($striped_mask, '!') === 0) {
            if (false === App::getModulesManager()->isInstance($value, str_replace('!', '', $striped_mask))) {
                return true;
            } else {
                return false;
            }
        }
        return App::getModulesManager()->isInstance($value, $striped_mask);
    }

    public function addListener(Listener $listener)
    {
        if (is_null($this->filtered_events)) {
            $this->filterEvents($listener->getName());
        }
        foreach ($this->filtered_events as $event) {
            $event->addListener($listener);
        }
    }

}
