<?php

namespace Aelast\Aecmf\Core\Events;

use Aelast\Aecmf\Core\Events\Event as Event;
use Aelast\Aecmf\Core\Events\EventsFilter as EventsFilter;

class EventsManager
{

    static private $instance;
    private $listeners = [];
    private $events = [];

    private function __construct()
    {
        
    }

    static public function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function normalizeEventsParams($params, $as_string = false)
    {
        if (is_string($params)) {
            $params = explode(Event::SEPARATOR, $params);
        }
        if (!is_array($params)) {
            $params = array();
        }
        while (count($params) < 4) {
            $params[] = '*';
        }
        while (count($params) > 4) {
            array_pop($params);
        }
        for ($i = 0; $i < count($params); $i++) {
            $params[$i] = trim(strtolower($params[$i]));
        }
        if ($as_string) {
            return implode(Event::SEPARATOR, $params);
        }
        return $params;
    }

    public function getEvents($params)
    {
        return new EventsFilter($this->events, $params);
    }

    public function getEvent($event_name)
    {
        if (isset($this->events[$event_name])) {
            return $this->events[$event_name];
        }
        return new Event('~.~.~.~');
    }

    public function setEvent(Event $event)
    {
        $this->events[$event->getName()] = $event;
        $this->applyListeners();
        return $this;
    }

    public function removeEvent($event_name)
    {
        if (isset($this->events[$event_name])) {
            unset($this->events[$event_name]);
        }
        return $this;
    }

    public function raiseEvent($params)
    {
        $event_name = $this->normalizeEventsParams($params, true);
        return $this->getEvent($event_name)->raise();
    }

    public function setListener($params, $callback, $module_name)
    {
        $listener_params = $this->normalizeEventsParams($params);
        $listener = new Listener($callback, $module_name, $listener_params);
        $this->listeners[$listener->getName()] = $listener;
        $this->applyListeners();
        return $this;
    }

    private function applyListeners()
    {
        foreach ($this->listeners as $listener) {
            $this->getEvents($listener->getParams())->addListener($listener);
        }
    }

}
