<?php

namespace Aelast\Aecmf\Core\Events;

class Event
{

    const BEFORE = 'before';
    const AFTER = 'after';
    const SEPARATOR = '.';

    protected $name;
    protected $listeners = array();
    protected $when = '';
    protected $module_name = '';
    protected $controller_name = '';
    protected $action_name = '';

    public function __construct($params)
    {
        if (is_string($params)) {
            $params = explode(self::SEPARATOR, $params);
        }
        if (!is_array($params) || count($params) != 4) {
            throw new \Core\CmfException('Incorrect event data');
        }
        $this->when = trim(strtolower(array_shift($params)));
        $this->module_name = trim(strtolower(array_shift($params)));
        $this->controller_name = trim(strtolower(array_shift($params)));
        $this->action_name = trim(strtolower(array_shift($params)));
        $this->name = implode(self::SEPARATOR, array(
            $this->when,
            $this->module_name,
            $this->controller_name,
            $this->action_name
        ));
    }

    public function addListener(Listener $listener)
    {
        $this->listeners[$listener->getName()] = $listener;
        return $this;
    }

    public function raise()
    {
        foreach ($this->listeners as $listener) {
            $result = $listener->run();
            if ($result === false) {
                break;
            }
        }
    }

    public function getName()
    {
        return $this->name;
    }

    public function getWhen()
    {
        return $this->when;
    }

    public function getModuleName()
    {
        return $this->module_name;
    }

    public function getControllerName()
    {
        return $this->controller_name;
    }

    public function getActionName()
    {
        return $this->action_name;
    }

    public function issetListener($listenerName)
    {
        return isset($this->listeners[$listenerName]);
    }

}
