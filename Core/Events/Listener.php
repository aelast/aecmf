<?php

namespace Aelast\Aecmf\Core\Events;

use Aelast\Aecmf\Core\Application as App;

class Listener
{

    private $module_name;
    private $callback;
    private $params;
    private $name;

    public function __construct($callback, $module_name, $params)
    {
        $this->callback = $callback;
        $this->module_name = $module_name;
        $this->params = $params;
        $this->name = md5(uniqid($this->module_name) . $this->module_name . json_encode($params));
    }

    public function run()
    {
        if (is_array($this->callback)) {
            return $this->runCallbackFromArray();
        }

        $callback = $this->callback;
        if (!is_callable($callback)) {
            return null;
        }
        return $callback($this->module_name);
    }

    private function runCallbackFromArray()
    {
        $array = $this->callback;
        $module_name = array_shift($array);
        $method_name = array_shift($array);
        $module = App::getModulesManager()->get($module_name);
        if (!method_exists($module, $method_name)) {
            return null;
        }
        return $module->$method_name($this->module_name);
    }

    public function getParams()
    {
        return $this->params;
    }

    public function getName()
    {
        return $this->name;
    }

}
