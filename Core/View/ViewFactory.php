<?php

namespace Aelast\Aecmf\Core\View;

class ViewFactory
{

    static public function getView($config)
    {
        return self::loadView($config);
    }

    static private function loadView($config)
    {
        if (empty($config['type'])) {
            return new View($config);
        }
        $class = __NAMESPACE__ . '\\' . ucfirst(strtolower($config['type'])) . 'View';
        if (class_exists($class)) {
            return new $class($config);
        } else {
            throw new \Core\CmfException('View class "' . $class . '" not found.');
        }
    }

}
