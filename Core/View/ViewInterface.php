<?php

namespace Aelast\Aecmf\Core\View;

interface ViewInterface
{

    public function isRendered();

    public function render();

    public function assign($name, $value);

    public function setLayout($layout);

    public function getLayout();

    public function setTemplate($template);

    public function getTemplate();

    public function getContent();

    public function getWidget($name, $params = array());
}
