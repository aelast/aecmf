<?php

namespace Aelast\Aecmf\Core\View;

use Aelast\Aecmf\Core\Application as App;
use Aelast\Aecmf\Core\View\WidgetInterface as WidgetInterface;
use Aelast\Aecmf\Core\View\View;

class Widget extends View implements WidgetInterface
{

    protected $view;
    protected $params;
    protected $uid = 'View.Widget';
    protected $parents = array();

    public function __construct(Array $params = array())
    {
        parent::__construct([]);
        $this->view = App::getView();
        $this->params = $params;
    }

    public function begin()
    {
        ob_start();
    }

    public function finish()
    {
        $this->content = ob_get_contents();
        ob_end_clean();
    }

    public function render()
    {
        $this->begin();
        if (!empty($this->template)) {
            $this->fetchTemplate();
        }
        $this->finish();
        $this->rendered = true;
    }

    private function fetchTemplate()
    {
        include($this->template);
    }

    public function getWidget($name, $params = array())
    {
        $widget = parent::getWidget($name, $params);
        if (in_array($widget->getUid(), $this->getParents())) {
            $widget = new Widget();
        }
        $widget->setParents($this->getParents());
        return $widget;
    }

    protected function getUid()
    {
        return $this->uid;
    }

    protected function setParents(Array $parents)
    {
        $this->parents = $parents;
    }

    protected function getParents()
    {
        $parents = $this->parents;
        $parents[] = $this->uid;
        return $parents;
    }

    public function __get($name)
    {
        return $this->view->get($name);
    }

    public function __call($name, $arguments)
    {
        return call_user_func(array($this->view, $name), $arguments);
    }

    public function __isset($name)
    {
        return isset($this->view->$name);
    }

}
