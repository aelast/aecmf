<?php

namespace Aelast\Aecmf\Core\View;

interface WidgetInterface
{

    public function begin();

    public function finish();
}
