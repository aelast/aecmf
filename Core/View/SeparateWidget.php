<?php

namespace Core\View;

use Aelast\Aecmf\Core\View\Widget;

class SeparateWidget extends Widget
{

    protected $uid = 'View.SeparateWidget';
    protected $response_assigned = true;

    public function __get($name)
    {
        if (!isset($this->params[$name])) {
            return null;
        }
        return $this->params[$name];
    }

    public function __call($name, $arguments)
    {
        unset($name, $arguments);
        return null;
    }

    public function __isset($name)
    {
        return isset($this->params[$name]);
    }

    public function __set($name, $value)
    {
        unset($name, $value);
        return false;
    }

}
