<?php

namespace Aelast\Aecmf\Core\View;

class Block
{

    const BLOCK_START = 'start';
    const BLOCK_READY = 'ready';
    const BLOCK_APPEND = 'append';
    const BLOCK_PREPEND = 'prepend';
    const BLOCK_REPLACE = 'replace';

    private $blocks = array();
    private $name = 'default';

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function open($type = self::BLOCK_APPEND)
    {
        $this->blocks[] = array('type' => $type, 'status' => self::BLOCK_START, 'content' => '');
        ob_start();
    }

    public function close($index = null)
    {
        if (is_null($index)) {
            $index = $this->getLastOpendBlockIndex();
        }
        if ($index === false) {
            return;
        }
        $this->blocks[$index]['content'] = ob_get_contents();
        $this->blocks[$index]['status'] = self::BLOCK_READY;
        ob_end_clean();
    }

    private function getLastOpendBlockIndex()
    {
        for ($i = count($this->blocks) - 1; $i >= 0; $i--) {
            if ($this->blocks[$i]['status'] === self::BLOCK_START) {
                return $i;
            }
        }
        return false;
    }

    public function render()
    {
        $content = '';
        $prev_block_type = self::BLOCK_APPEND;
        for ($i = 0; $i < count($this->blocks); $i++) {
            if ($prev_block_type === self::BLOCK_APPEND) {
                $content = $this->blocks[$i]['content'] . $content;
            } elseif ($prev_block_type === self::BLOCK_PREPEND) {
                $content .= $this->blocks[$i]['content'];
            }
            if ($this->blocks[$i]['type'] === self::BLOCK_REPLACE) {
                break;
            }
            $prev_block_type = $this->blocks[$i]['type'];
        }
        $this->clear();
        echo $content;
    }

    private function clear()
    {
        $this->blocks = array();
    }

}
