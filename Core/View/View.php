<?php

namespace Aelast\Aecmf\Core\View;

use Aelast\Aecmf\Core\Application as App;
use Aelast\Aecmf\Core\Config\Config as Config;
use Aelast\Aecmf\Core\View\Widget as Widget;
use Aelast\Aecmf\Core\View\Block as Block;
use Aelast\Tools\Strings as Strings;

class View implements ViewInterface
{

    protected $config;
    protected $layout;
    protected $template;
    protected $vars = array();
    protected $content = '';
    protected $rendered = false;
    protected $response_assigned = false;
    protected $response;
    protected $blocks = array();
    protected $blocks_stack = array();

    public function __construct($config)
    {
        $this->config = $config;
        $this->response = App::getResponse();
        $this->view = $this;
    }

    public function render()
    {
        if (!$this->response_assigned) {
            $this->assignResponseVars();
        }
        if (empty($this->template)) {
            throw new \Core\CmfException('Layout or template not exists');
        } else {
            $this->fetch($this->template);
        }
        if (!empty($this->layout)) {
            $this->fetch($this->layout);
        }
        $this->rendered = true;
    }

    public function show()
    {
        if (!$this->rendered) {
            $this->render();
        }
        echo $this->getContent();
    }

    protected function assignResponseVars()
    {
        foreach ($this->response as $name => $value) {
            $this->set($name, $value);
        }
        $this->assign('errors', $this->response->getErrors())->
            assign('warnings', $this->response->getWarnings())->
            assign('messages', $this->response->getMessages())->
            assign('template_path', $this->config['template_path'])->
            assign('assets_path', $this->config['assets_path']);
        $this->setTemplate($this->response->template);
        $layout = $this->response->layout;
        if (empty($layout)) {
            $layout = $this->config['layout'];
        }
        $this->setLayout($layout);
        $this->response_assigned = true;
    }

    private function fetch($template)
    {
        ob_start();
        include($template);
        $this->content = ob_get_contents();
        ob_end_clean();
    }

    public function isRendered()
    {
        return $this->rendered;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function getApplication()
    {
        return $this->app;
    }

    public function setLayout($layout)
    {
        $this->layout = $layout;
        return $this;
    }

    public function setTemplate($template)
    {
        $this->template = $template;
        return $this;
    }

    public function getLayout()
    {
        return $this->layout;
    }

    public function getTemplate()
    {
        return $this->template;
    }

    public function escape($string, $type = 'html', $charset = null, $double = true)
    {
        if (is_null($charset)) {
            $charset = Config::get('charset', 'UTF-8');
        }
        return Strings::escape($string, $type, $charset, $double);
    }

    public function set($name, $value)
    {
        $this->vars[$name] = $value;
        return $this;
    }

    public function __set($name, $value)
    {
        return $this->set($name, $value);
    }

    public function assign($name, $value)
    {
        return $this->set($name, $value);
    }

    public function get($name, $default = null)
    {
        if (!isset($this->vars[$name])) {
            return $default;
        }
        return $this->vars[$name];
    }

    public function __get($name)
    {
        return $this->get($name);
    }

    public function __call($name, $arguments)
    {
        if (strpos($name, 'get') !== 0 && strpos($name, 'set') !== 0) {
            throw new \Core\CmfException('Method "' . $name . '" not exists');
        }
        $segments = preg_split('/(?=[A-Z])/', $name);
        array_shift($segments);
        $var_name = strtolower(implode('_', $segments));
        if (strpos($name, 'get') === 0) {
            return $this->get($var_name);
        } elseif (strpos($name, 'set') === 0) {
            return $this->set($var_name, isset($arguments[0]) ? $arguments[0] : null);
        }
    }

    public function __isset($name)
    {
        return isset($this->vars[$name]);
    }

    public function openBlock($name, $type = Block::BLOCK_APPEND)
    {
        if (!isset($this->view->blocks[$name])) {
            $this->view->blocks[$name] = new Block($name);
        }
        $this->view->blocks[$name]->open($type);
        $this->view->blocks_stack[] = $this->view->blocks[$name];
    }

    public function closeBlock($render = false)
    {
        if (count($this->view->blocks_stack) == 0) {
            return;
        }
        end($this->view->blocks_stack)->close();
        if ($render === true) {
            end($this->view->blocks_stack)->render();
        }
    }

    public function removeBlock($name)
    {
        $this->openBlock($name, Block::BLOCK_REPLACE);
        $this->closeBlock();
    }

    public function declareBlock($name)
    {
        $this->openBlock($name);
        $this->closeBlock(true);
    }

    public function getWidget($name, $params = array())
    {
        $segments = explode('.', $name);
        if (count($segments) < 2) {
            return new Widget();
        }
        $module_name = ucfirst($segments[0]);
        $widget_name = ucfirst($segments[1]);
        try {
            $widget = App::getModulesManager()->get($module_name)->getWidget($widget_name, $params);
        } catch (\Exception $error) {
            $widget_errors = $this->get('widget_errors', array());
            $widget_errors[] = array($name => $error->getMessage());
            $this->assign('widget_errors', $widget_errors);
            $widget = false;
        }
        if (!is_object($widget)) {
            return new Widget();
        }
        return $widget;
    }

    public function showWidget($name, $params = array())
    {
        $this->getWidget($name, $params)->show();
    }

}
