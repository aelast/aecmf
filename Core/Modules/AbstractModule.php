<?php

namespace Aelast\Aecmf\Core\Modules;

use Aelast\Aecmf\Core\Application as App;
use Aelast\Aecmf\Core\Config\Config as Config;
use Aelast\Aecmf\Core\Components\ComponentsLoader as Loader;

abstract class AbstractModule
{

    protected $response;
    protected $initialized = false;
    protected $name = '';
    protected $config;
    protected $info;
    protected $controllers = [];
    protected $services = [];
    protected $views = [];

    public function __construct($info)
    {
        $this->info = $info;
        $this->response = App::getResponse();
        $this->loader = new Loader($this);
        $this->config = Config::asObject([
                App::getType(),
                'modules.' . $this->getName(),
        ]);
    }

    public function initialize()
    {
        if ($this->initialized) {
            return false;
        }
        $this->initialized = true;
    }

    public function isInitialized()
    {
        return $this->initialized;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPath()
    {
        return $this->info->path;
    }

    public function getConfig()
    {
        if (is_null($this->config)) {
            $this->loadConfig();
        }
        return $this->config;
    }

    public function getController($name)
    {
        $controller_name = ucfirst($name);
        if (!isset($this->controllers[$controller_name])) {
            $controller = $this->loader->loadComponent('controller', $controller_name);
            if ($controller === false) {
                throw new \Exception('Controller "' . $controller_name . '" not found in module "' . $this->getName() . '"');
            }
            $this->controllers[$controller_name] = $controller;
        }
        return $this->controllers[$controller_name];
    }

    public function getModel($name)
    {
        $model_name = ucfirst($name);
        if (!isset($this->models[$model_name])) {
            $model = $this->loader->loadComponent('model', $model_name);
            if ($model === false) {
                throw new \Exception('Model "' . $model_name . '" not found in module "' . $this->getName() . '"');
            }
            $this->models[$model_name] = $model;
        }
        return $this->models[$model_name];
    }

    public function getService($name)
    {
        $service_name = ucfirst($name);
        if (!isset($this->services[$service_name])) {
            $service = $this->loader->loadComponent('service', $service_name);
            if ($service === false) {
                throw new \Exception('Service "' . $service_name . '" not found in module "' . $this->getName() . '"');
            }
            $this->services[$service_name] = $service;
        }
        return $this->services[$service_name];
    }

    public function getWidget($name, $params)
    {
        $widget_name = ucfirst($name);
        $widget = $this->loader->loadComponent('widget', $widget_name, $params);
        if ($widget === false) {
            throw new \Exception('Widget "' . $widget_name . '" not found in module "' . $this->getName() . '"');
        }
        return $widget;
    }

}
