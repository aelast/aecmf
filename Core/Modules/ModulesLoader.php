<?php

namespace Aelast\Aecmf\Core\Modules;

use Aelast\Aecmf\Core\Application as App;
use Aelast\Aecmf\Core\Config\Config as Config;
use Aelast\Tools\Files as Files;

class ModulesLoader
{

    private static $instance;
    private $modules;

    private function __construct()
    {
        $this->loadModulesList();
        $this->loadModulesConfig();
    }

    static public function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Получение списка возможных модулей
     * из папки Modules
     */
    private function loadModulesList()
    {
        $dirs = Files::getList('../Modules/', false, true);
        if (!is_array($dirs)) {
            $dirs = array();
        }
        $modules = array();
        foreach ($dirs as $moduleDir) {
            $module = $this->loadModuleInfo($moduleDir);
            if ($module !== false) {
                $modules[$module->name] = $module;
            }
        }
        $this->modules = $modules;
    }

    /**
     * Получение информации о модуле
     * 
     * @param type $module_path
     * @return boolean|\stdClass
     */
    private function loadModuleInfo($module_path)
    {
        $module = new \stdClass();
        $module->path = $module_path['fullname'];
        $module->name = $module_path['basename'];
        $module->file = $module_path['fullname'] . '/' . $module->name . '.php';
        if (!file_exists($module->file)) {
            unset($module);
            return false;
        }
        $module->namespace = 'Aelast\\Aecmf\\Modules\\' . $module->name;
        $module->class = $module->namespace . '\\' . $module->name;
        if (file_exists($module_path['fullname'] . '/config.php')) {
            $module->config_file = $module_path['fullname'] . '/config.php';
        }
        return $module;
    }

    private function loadModulesConfig()
    {
        foreach ($this->modules as $module) {
            $this->loadModuleConfig($module);
        }
    }

    private function loadModuleConfig($module)
    {
        if (!isset($module->config_file)) {
            return;
        }
        Config::load(require $module->config_file);
        $this->setModuleListeners($module);
    }

    private function setModuleListeners($module)
    {
        $listeners = Config::get([
                'modules.' . $module->name . '.listeners',
                'modules.' . $module->name . '.' . App::getType() . '.listeners'
        ]);
        foreach ($listeners as $listener) {
            App::getEventsManager()->setListener(
                $listener['event'], $listener['action'], $module->name
            );
        }
    }

    private function loadModule($name)
    {
        $module = $this->getModuleInfo($name);
        require_once $module->file;
        if (class_exists($module->class, false)) {
            return new $module->class($module);
        }
        return false;
    }

    public function getModuleInfo($name)
    {
        if (!isset($this->modules[$name])) {
            return false;
        }
        return $this->modules[$name];
    }

    public function getModuleInstance($name)
    {
        $module = $this->getModuleInfo($name);
        if ($module === false) {
            return false;
        }
        return $this->loadModule($name);
    }

}
