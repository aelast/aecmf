<?php

namespace Aelast\Aecmf\Core\Modules;

interface ModuleInterface
{

    public function initialize();

    public function getConfig();

    public function getName();

    public function getController($controllerName);
}
