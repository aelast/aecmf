<?php

namespace Aelast\Aecmf\Core\Modules;

use Aelast\Tools\Strings as Strings;
use Aelast\Aecmf\Core\Modules\ModulesLoader as Loader;

class ModulesManager
{

    static private $instance;
    private $modules = array();
    private $loader;

    private function __construct()
    {
        $this->loader = Loader::getInstance();
    }

    static public function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Получение объекта модуля
     * 
     * @param string $name
     * @return Instance of Module
     * @throws \Aelast\Aecmf\Core\CmfException
     */
    public function get($name)
    {
        $module_name = Strings::normalizeName($name);
        if (!isset($this->modules[$module_name])) {
            $this->modules[$module_name] = $this->loader->getModuleInstance($module_name);
        }
        if ($this->modules[$module_name] === false) {
            throw new \Aelast\Aecmf\Core\CmfException('Module "' . $module_name . '" not exists');
        }
        if (!$this->modules[$module_name]->isInitialized()) {
            $this->modules[$module_name]->initialize();
        }
        return $this->modules[$module_name];
    }

    public function getModuleInfo($name)
    {
        $moduleName = Strings::normalizeName($name);
        return $this->loader->getModuleInfo($moduleName);
    }

}
