<?php

namespace Aelast\Aecmf\Core;

class DataStore
{

    /**
     * Флаг замены данных, который может быть установлен в массиве 
     * При объединении с новым массивом данных, данные в исходном массиве, 
     * будут заменены, данными из массива источника, при совпадении ключа, следующего за этим флагом
     * 
     */
    const FLAG_REPLACE_AFTER = '@flag:replace_after';

    /**
     * Флаг замены данных, который может быть установлен в массиве
     * При объединении с новым массивом данных, данные в исходном массиве, 
     * будут заменены, данными из массива источника, при совпадении ключей
     */
    const FLAG_REPLACE_ALL = '@flag:replace_all';

    /**
     * Флаг возможности удаления данных, который может быть установлен в массиве
     * При необходимости удалить данные, они могут быть удалены, если в массиве 
     * будет найден указанный флаг
     */
    const FLAG_REMOVE = '@flag:removable';

    /**
     * Массив данных
     * 
     * @var array $data Массив данных 
     */
    private $data = [];

    /**
     * конструктор класса
     * @param array $data
     */
    public function __construct($data = [])
    {
        $this->set($data);
    }

    /**
     * Добавление массива элементов в массив
     * 
     * @param arary $data
     */
    public function set($data)
    {
        $this->data = $this->mergeData($this->data, $this->prepareData($data));
        return $this;
    }

    /**
     * Добавление массива элементов в массив
     * 
     * @param arary $data
     */
    public function add($name, $value)
    {
        $this->data = $this->mergeData($this->data, $this->prepareData([$name => $value]));
        return $this;
    }

    /**
     * Метод обработки массива данных
     * Разбивает сегментные ключи массива и создает структуру массива
     * 
     * @param type $data
     * @return array обработанный массив данных конфигурации
     */
    private function prepareData($data)
    {
        if (!is_array($data)) {
            $data = [$data];
        }
        $result = array();
        foreach ($data as $key => $value) {
            $value = is_array($value) ? $this->prepareData($value) : $value;
            if (strpos($key, '.') !== false) {
                $result = $this->mergeData($result, $this->createArrayFromSegments($key, $value));
            } else {
                $result[$key] = $value;
            }
        }
        return $result;
    }

    /**
     * Метод создающий массив на основании сегментированного ключа
     * 
     * Если ключ содержит точку, он будет разбит на массив по точке
     * Каждый элемент массива - это ключ
     * this.is.segmented.key будет преобразован в массив
     * array['this']['is']['segmented']['key'] 
     * Последнему вложенному элементу будет присвоено значение $value
     * 
     * @param string $path - сегментированный ключ
     * @param mixed $value - значение последнего элемента
     * @return array ассоциативный массив
     * 
     */
    private function createArrayFromSegments($path, $value)
    {
        $segments = explode('.', $path);
        $result = array();
        $r = &$result;
        foreach ($segments as $segment) {
            $r[$segment] = array();
            $r = &$r[$segment];
        }
        $r = $value;
        return $result;
    }

    /**
     * Объединяет 2 массива в один
     * 
     * Если в массиве-цели существует ключ, и данные в этом массиве - массив,
     * будет произведено рекурсивное объединение массивов.
     * Если в массиве-цели существует ключ, и данные в этом массиве не массив,
     * эти данные будут проигнорированны.
     * Если в массиве-источнике будет встречен флаг self::FLAG_REPLACE_AFTER
     * следующий элемент массива будет заменен
     * Если в массиве-источнике будет встречен флаг self::FLAG_REPLACE_ALL
     * все данные будут заменены
     * 
     * @param array $target - массив-цель
     * @param array $source - массив-источник
     * 
     * @return array $target
     */
    private function mergeData($target, $source)
    {
        $replace = !empty($source[static::FLAG_REPLACE_ALL]);
        $replaceOnce = false;
        foreach ($source as $key => $value) {
            if (strpos($key, static::FLAG_REPLACE_AFTER) !== false) {
                $replaceOnce = true;
            }
            // Просто заменим/добавим данные, если такого ключа нет
            // или указан флаг замены
            if (!isset($target[$key]) || $replace || $replaceOnce) {
                $target[$key] = $value;
            }
            // Если ключ есть и данные и в нем и в источнике - массивы
            // Объединим их
            elseif (isset($target[$key]) && is_array($target[$key]) && is_array($value)) {
                $target[$key] = $this->mergeData($target[$key], $value);
            }
            if (strpos($key, static::FLAG_REPLACE_AFTER) === false && $replaceOnce === true) {
                $replaceOnce = false;
            }
        }
        return $target;
    }

    /**
     * Метод получения данных
     * 
     * Возвращает данные в том виде, в котором они есть
     * по ключу или сегментированному пути или $default, если путь или ключ не найден
     * 
     * @param string $name индекс или сегментированный путь
     * @param mixed $default данные, которые возвращаются, если указанный ключ не найден
     * @return mixed значение по ключу или сегментированному пути массива
     */
    public function get($name, $default = null)
    {
        if (strpos($name, '.') === false) {
            if (isset($this->data[$name])) {
                return $this->data[$name];
            }
            return $default;
        }
        $segments = explode('.', $name);
        $foundedValue = $this->data;
        foreach ($segments as $segment) {
            if (isset($foundedValue[$segment])) {
                $foundedValue = $foundedValue[$segment];
                continue;
            } else {
                return $default;
            }
        }
        return $foundedValue;
    }

    public function getSegments($names)
    {
        $results = [];
        foreach ($names as $name) {
            $result = $this->get($name, []);
            $results = $this->mergeData($results, $result);
        }
        return $results;
    }

    /**
     * Метод получения данных в виде объекта \Core\DataStore
     * 
     * Возвращает данные в виде объекта по ключу или сегментированному пути
     * или $default, если путь или ключ не найден
     * Если найденный элемент не является массивом, будет возвращен объект с 
     * массивом, содержащим найденный элемент с индексом $name
     * 
     * @param mixed $names индекс, массив или сегментированный путь
     * @param mixed $default данные, которые будут использованны, если указанный ключ не найден
     * @return \Core\Config\Config новый экземпляр класса
     */
    public function asObject($names, $default = null)
    {
        if (is_array($names)) {
            $value = $this->getSegments($names);
        } else {
            $value = $this->get($names, $default);
        }
        if (!is_array($value)) {
            return new self([$names => $value]);
        }
        return new self($value);
    }

    /**
     * Метод возвращает весь массив в том виде, в котором он есть
     * 
     * @return array все данные
     */
    public function getAll()
    {
        return $this->data;
    }

    /**
     * Метод удаляет секцию конфигурации
     * 
     * Метод удаляет секцию конфигурации с указанным ключем, если в этой секции содержится 
     * элемент с флагом FLAG_REMOVE
     * 
     * @param string $name - ключ
     * @return \Core\Config\Config
     */
    public function remove($name)
    {
        if (!empty($this->data[$name][static::FLAG_REMOVE])) {
            unset($this->data[$name]);
        }
        return $this;
    }

    /**
     * Метод доступа к секции, как к свойству объекта
     * 
     * @param string $name ключ
     * @return mixed
     */
    public function __get($name)
    {
        return $this->get($name);
    }

    /**
     * Метод установки данных, как свойства объекта
     * 
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $this->add($name, $value);
    }

    /**
     * Метод проверки наличия секции по ключу массива
     * 
     * @param string $name
     * @return bool
     */
    public function __isset($name)
    {
        return isset($this->data[$name]);
    }

}
