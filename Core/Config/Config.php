<?php

namespace Aelast\Aecmf\Core\Config;

use Aelast\Aecmf\Core\DataStore as Store;

/**
 * Класс кофигурации
 * Выполняет хранение, объединение, поиск конфигурации системы из массивов
 * @param array $data массив конфигурации системы
 * 
 */
class Config
{

    static $instance;

    /**
     * Метод загрузки массива конфигурации
     * 
     * Переданный массив будет добавлен в существующую конфигурацию
     * 
     * @param array $data
     *
     */
    public static function load($data = [])
    {
        if (!isset(static::$instance)) {
            static::$instance = new Store();
        }
        static::$instance->set($data);
    }

    public static function get($names, $default = null)
    {
        if (is_array($names)) {
            return static::$instance->getSegments($names);
        }
        return static::$instance->get($names, $default);
    }

    public static function asObject($names, $default = null)
    {
        return static::$instance->asObject($names, $default);
    }

    public static function remove($name)
    {
        static::$instance->remove($name);
    }

}
