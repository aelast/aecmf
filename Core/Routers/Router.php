<?php

namespace Aelast\Aecmf\Core\Routers;

class Router
{

    const URI_RELATIVE = 'relative';
    const URI_ABSOLUTE = 'active';

    private static $instance;

    public static function init(Array $config)
    {
        if (empty($config['type'])) {
            throw new \Exception('Router type not declared');
        }
        $class = __NAMESPACE__ . '\\' . ucfirst(strtolower($config['type'])) . 'Router';
        if (class_exists($class)) {
            static::$instance = new $class($config);
            return static::$instance;
        } else {
            throw new \Exception('Router class "' . $class . '" not found.');
        }
    }

    public static function getModule()
    {
        return static::$instance->getModule();
    }

    public static function getController()
    {
        return static::$instance->getController();
    }

    public static function getAction()
    {
        return static::$instance->getAction();
    }

    public static function redirect($path, $arguments = null)
    {
        static::$instance->redirect($path, $arguments);
    }

    public static function getRoute($route = null)
    {
        if (is_null($route)) {
            return static::$instance->getRoute();
        }
        return static::$instance->buildRoute($route);
    }

    public static function build($path = null, $params = null, $separator = '&')
    {
        return static::$instance->build($path, $params, $separator);
    }

}
