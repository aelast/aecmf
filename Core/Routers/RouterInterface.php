<?php

namespace Aelast\Aecmf\Core\Routers;

interface RouterInterface {
    public function buildRoute($path = null);
}
