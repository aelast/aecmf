<?php

namespace Aelast\Aecmf\Core\Routers;

use Aelast\Aecmf\Core\Routers\Router as Router;
use Aelast\Aecmf\Core\Components\Request as Request;
use Aelast\Aecmf\Core\DataStore as Store;

class AbstractRouter
{

    const SEPARATOR = '/';

    protected $url;
    protected $config;
    protected $module;
    protected $controller;
    protected $action;
    protected $default_module;
    protected $default_controller;
    protected $default_action;

    public function __construct(Array $config)
    {
        $this->config = new Store($config);
        $this->default_module = $this->config->get('default_module', 'default');
        $this->default_controller = $this->config->get('default_controller', 'default');
        $this->default_action = $this->config->get('default_action', 'index');
    }

    public function getModule()
    {
        return ucfirst($this->module);
    }

    public function getController()
    {
        return ucfirst($this->controller);
    }

    public function getAction()
    {
        return ucfirst($this->action);
    }

    /**
     * Возвращает базовый URL приложения
     * 
     * @return string url
     */
    public function getBaseUrl()
    {
        $url = ($this->config->get('uri_type', Router::URI_RELATIVE) == Router::URI_ABSOLUTE ?
                Request::getProtocol() . '://' . Request::getHost() : '')
            . Request::getBasePath()
            . self::SEPARATOR;
        return $url;
    }

    /**
     * Возвращает query строку
     * 
     * @param array $params
     * @param string $separator
     * @return string
     */
    public function buildQuery($params = null, $separator = '&')
    {
        if (is_array($params)) {
            return http_build_query($params, '', $separator);
        }
        return '';
    }

    /**
     * Возвращает URL
     * 
     * @param string $path
     * @param array $params
     * @param string $separator
     * @return string
     */
    public function build($path = null, $params = null, $separator = '&')
    {
        $url = $this->getBaseUrl() . $this->buildRoute($path);
        if (is_array($params) && count($params) > 0) {
            $queryString = $this->buildQuery($params, $separator);
            $url .= (strpos($url, '?') === false ? '?' : $separator) . $queryString;
        }
        return $url;
    }

    /**
     * Возвращает URL текущей страницы.
     * Параметры в query string могут быть дополнены или заменены значениями из $params
     * 
     * @param array $params
     * @param string $separator
     * @return string
     */
    public function url($params = null, $separator = '&')
    {
        $queryParams = $this->request->getQueryParams();
        if (is_array($params)) {
            foreach ($params as $key => $value) {
                if ((is_null($value) || strlen($value) == 0) && isset($queryParams[$key])) {
                    unset($queryParams[$key]);
                    continue;
                }
                $queryParams[$key] = $value;
            }
        }
        $url = $this->build(null, $queryParams, $separator);
        return $url;
    }

    /**
     * Выполняет редирект
     * 
     * @param type $path
     * @param type $arguments
     */
    public function redirect($path, $arguments = null)
    {
        $url = $this->build($path, $arguments);
        Header('Location: ' . $url);
        exit();
    }

}
