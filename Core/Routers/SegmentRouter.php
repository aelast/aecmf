<?php

namespace Aelast\Aecmf\Core\Routers;

use Aelast\Aecmf\Core\Routers\RouterInterface;
use Aelast\Aecmf\Core\Routers\AbstractRouter;
use Aelast\Aecmf\Core\Components\Request as Request;

class SegmentRouter extends AbstractRouter implements RouterInterface
{

    const DEFAULT_ROUTE_STRING = parent::SEPARATOR;

    private $aliases = [];
    private $route_string = '';

    public function __construct(Array $config)
    {
        parent::__construct($config);
        $this->aliases = $this->config->get('aliases', []);
        $this->init();
    }

    private function init()
    {
        $url = trim(substr_replace(Request::getPath(), '', 0, strlen(Request::getBasePath())), parent::SEPARATOR);
        $this->route_string = empty($url) ? self::DEFAULT_ROUTE_STRING : $url;
        $this->extractSegments();
    }

    private function extractSegments()
    {
        $route_string = isset($this->aliases[$this->route_string]) ? $this->aliases[$this->route_string] : $this->route_string;
        $segments = explode(parent::SEPARATOR, $route_string);

        $this->module = !empty($segments[0]) ? strtolower($segments[0]) : strtolower($this->default_module);
        $this->controller = !empty($segments[1]) ? strtolower($segments[1]) : strtolower($this->default_controller);
        $this->action = !empty($segments[2]) ? strtolower($segments[2]) : strtolower($this->default_action);
    }

    private function prepareRouteString($path)
    {
        if (!is_array($path)) {
            $route_string = isset($this->aliases[$path]) ? $this->aliases[$path] : $path;
            $segments = explode(parent::SEPARATOR, $route_string);
        }
        $segments[0] = !empty($segments[0]) ? strtolower($segments[0]) : $this->default_module;
        $segments[1] = !empty($segments[1]) ? strtolower($segments[1]) : $this->default_controller;
        $segments[2] = !empty($segments[2]) ? strtolower($segments[2]) : $this->default_action;
        return implode(parent::SEPARATOR, $segments);
    }

    public function buildRoute($path = null)
    {
        $route_string = is_null($path) ? $this->route_string : $this->prepareRouteString($path);
        $aliases = array_flip($this->aliases);
        if (isset($aliases[$route_string])) {
            $route_string = $aliases[$route_string];
        }
        if ($route_string != parent::SEPARATOR && $route_string != '') {
            return $route_string . parent::SEPARATOR;
        }
        return '';
    }

    public function getRoute($separator = parent::SEPARATOR, $lower = true)
    {
        $path = $this->getModule() . $separator .
            $this->getController() . $separator .
            $this->getAction();
        if ($lower) {
            return strtolower($path);
        }
        return $path;
    }

}
